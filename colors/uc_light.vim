" ------------------------------------------------------------------------------
" Description: Vim colorscheme uc_light
" Author: 3d_immortal <3d.immortal@gmail.com>
"
" Initially created with the help of ThemeCreator
" (https://github.com/mswift42/themecreator), but then later heavily modified.
" ------------------------------------------------------------------------------

" Init: {{{

set background=light
highlight clear
if exists("syntax_on")
    syntax reset
endif

set t_Co=256
let g:colors_name = "uc_light"

" }}}
" Functions: {{{

" TODO: Cleanup and unify these functions.
function! s:hif(group, foreground)
  exec  "highlight "  . a:group
    \ . " guifg="     . a:foreground
endfunction

function! s:hifs(group, foreground, fontStyle)
  exec  "highlight "  . a:group
    \ . " guifg="     . a:foreground
    \ . " gui="       . a:fontStyle
    \ . " cterm="     . a:fontStyle
endfunction

function! s:hi(group, foreground, background, fontStyle)
  exec  "highlight "  . a:group
    \ . " guifg="     . a:foreground
    \ . " guibg="     . a:background
    \ . " gui="       . a:fontStyle
    \ . " cterm="     . a:fontStyle
endfunction

" }}}
" Colors: {{{

let s:bg="#ffffff"
let s:fg="#000000"
let s:fg2="#141414"
let s:fg3="#292929"
let s:fg4="#3d3d3d"
let s:bg2="#ebebeb"
let s:bg3="#d6d6d6"
let s:bg4="#c2c2c2"
let s:bg5="#E8F2FE"
let s:keyword="#02A802"
let s:builtin="#8D3482"
let s:const= "#ff5f00"
let s:comment="#3F7F5F"
let s:func="#0000FF"
let s:str="#FF0000"
let s:type="#004d17"
let s:var="#800000"
let s:error="#ff0000"
let s:warning="#ff8800"
let s:my_aqua="#1E90FF"
let s:enum="#9F3CDC"
let s:field="#3465A4"
let s:dark_sea_green = '#5faf87'
let s:deep_sky_blue = '#00afff'
let s:steel_blue = '#5fafff'
let s:light_golden_rod = '#ffff5f'
let s:macro="#87afff"
let s:enum_type="#875f00"

" }}}
" Text Styles: {{{

let s:bold = 'bold,'
let s:italic = 'italic,'
let s:underline = 'underline,'
let s:undercurl = 'undercurl,'
let s:none = 'NONE'

" }}}
" Normal: {{{

exe 'hi Normal guifg='s:fg' guibg='s:bg
exe 'hi Cursor guifg='s:bg' guibg='s:fg
exe 'hi CursorLine  guibg='s:bg5
exe 'hi CursorLineNr  guibg='s:bg5
call s:hi('CursorLineNr', s:const, s:bg5, s:none)
exe 'hi CursorColumn  guibg='s:bg5
exe 'hi ColorColumn  guibg='s:bg2
exe 'hi LineNr guifg='s:fg2' guibg='s:bg2
exe 'hi VertSplit guifg='s:bg' guibg='s:fg3
exe 'hi MatchParen guifg='s:warning'  gui=underline'
call s:hi('StatusLine', s:fg2, s:bg3, s:bold)
call s:hi('StatusLineNC', s:fg3, s:bg4, s:bold)
exe 'hi Pmenu guifg='s:fg' guibg='s:bg2
exe 'hi PmenuSel  guibg='s:bg3
exe 'hi IncSearch guifg='s:bg' guibg='s:keyword
exe 'hi Search   gui=underline'
exe 'hi Directory guifg='s:const
exe 'hi Folded guifg='s:fg4' guibg='s:bg2
exe 'hi Boolean guifg='s:const
exe 'hi Character guifg='s:const
exe 'hi Comment guifg='s:comment
exe 'hi Conditional guifg='s:keyword
call s:hi('Conditional', s:keyword, s:none, s:bold)
exe 'hi Constant guifg='s:const
exe 'hi Define guifg='s:builtin
call s:hifs('Include', s:builtin, s:bold)
call s:hifs('Macro', s:builtin, s:none)
exe 'hi DiffAdd guifg=#000000 guibg=#bef6dc gui=bold'
exe 'hi DiffDelete guifg='s:bg2
exe 'hi DiffChange  guibg=#5b76ef guifg=#ffffff'
exe 'hi DiffText guifg=#ffffff guibg=#ff0000 gui=bold'
exe 'hi ErrorMsg guifg='s:error' guibg='s:bg2' gui=bold'
exe 'hi WarningMsg guifg='s:fg' guibg='s:warning
exe 'hi Float guifg='s:const
call s:hi('Function', s:my_aqua, s:none, s:none)
exe 'hi Identifier guifg='s:var'  gui=italic'
exe 'hi Keyword guifg='s:keyword'  gui=bold'
exe 'hi Label guifg='s:var
exe 'hi NonText guifg='s:bg4' guibg='s:bg2
exe 'hi Number guifg='s:const
exe 'hi Operator guifg='s:const
exe 'hi PreProc guifg='s:builtin
exe 'hi Special guifg='s:const
exe 'hi SpecialKey guifg='s:fg2' guibg='s:bg2
exe 'hi Statement guifg='s:keyword
exe 'hi StorageClass guifg='s:keyword'  gui=italic'
exe 'hi String guifg='s:str
exe 'hi Tag guifg='s:keyword
exe 'hi Title guifg='s:fg'  gui=bold'
exe 'hi Todo guifg='s:fg2'  gui=inverse,bold'
exe 'hi Type guifg='s:type
exe 'hi Structure guifg='s:keyword
exe 'hi Typedef guifg='s:keyword
exe 'hi Underlined   gui=underline'
exe 'hi Delimiter guifg='s:var

" Custom groups
exe 'hi Field guifg='s:field
exe 'hi FuncCall guifg='s:func' gui=bold'
exe 'hi UserType guifg='s:type' gui=bold'
exe 'hi EnumConst guifg='s:enum
exe 'hi MacroConst guifg='s:macro
exe 'hi EnumType guifg='s:enum_type' gui=bold'

" }}}
" Ruby Highlighting: {{{

exe 'hi rubyAttribute guifg='s:builtin
exe 'hi rubyLocalVariableOrMethod guifg='s:var
exe 'hi rubyGlobalVariable guifg='s:var' gui=italic'
exe 'hi rubyInstanceVariable guifg='s:var
exe 'hi rubyKeyword guifg='s:keyword
exe 'hi rubyKeywordAsMethod guifg='s:keyword' gui=bold'
exe 'hi rubyClassDeclaration guifg='s:keyword' gui=bold'
exe 'hi rubyClass guifg='s:keyword' gui=bold'
exe 'hi rubyNumber guifg='s:const

" }}}
" Python Highlighting: {{{

exe 'hi pythonBuiltinFunc guifg='s:builtin

" }}}
" Go Highlighting: {{{

exe 'hi goBuiltins guifg='s:builtin

" }}}
" Javascript Highlighting: {{{

exe 'hi jsBuiltins guifg='s:builtin
exe 'hi jsFunction guifg='s:keyword' gui=bold'
exe 'hi jsGlobalObjects guifg='s:type
exe 'hi jsAssignmentExps guifg='s:var

" }}}
" Html Highlighting: {{{
exe 'hi htmlLink guifg='s:var' gui=underline'
exe 'hi htmlStatement guifg='s:keyword
exe 'hi htmlSpecialTagName guifg='s:keyword

" }}}
" Markdown Highlighting: {{{

exe 'hi mkdCode guifg='s:builtin

" }}}
" Squiglies: {{{

exe 'hi SpellCap gui='s:undercurl' guisp='s:error
exe 'hi SpellBad gui='s:undercurl' guisp='s:error
exe 'hi ALEError gui='s:undercurl' guisp='s:error
exe 'hi ALEWarning gui='s:undercurl' guisp='s:warning
exe 'hi ALEInfo gui='s:undercurl' guisp='s:my_aqua
exe 'hi ALEInfoSign guifg='s:my_aqua' guibg='s:bg2' gui=bold'
hi! link ALEErrorSign Error
hi! link ALEWarningSign Todo


" }}}
" C/C++ and D: {{{

" Add more syntax for cpp and D (inspired by purify).
function! s:add_common_syntax()
  syn match cppBraces       "[{}\[\]]"
  syn match cppParens       "[()]"
  syn match cppOpSymbols    "=\{1,2}\|!=\|<\|>\|>=\|<=\|++\|+=\|--\|-=\|-"
  syn match cppOpSymbols    "\(\*\)\|\(|\)\|\(&\)\|\(?\)\|\(+\)"
  syn match cppOpSymbols   "\v\s/\s"
  syn match cppEndColons    "[,:;]"
  syn match cppLogicSymbols "\(&&\)\|\(||\)\|\(!\)"

  call s:hif('cppBraces', s:var)
  call s:hif('cppParens', s:var)
  call s:hif('cppOpSymbols', s:const)
  call s:hif('cppEndColons', s:const)
  call s:hif('cppLogicSymbols', s:const)
endfunction

function! s:add_more_cpp_syntax()
    syn match skiaFuncCall "\v<\l\w*\ze\("
    syn match cppField "\v\w(\w)*_>"
    syn match cppFunctionCall "\v<\u\w*\ze\("
    syn match cppConstant "\v<k(\u|\d)\w*>"
    syn match cppAccessor "\zs\(\(\l*_*\)*\l\)\ze("
    syn match enumConsts "\u\(\u\|_\|\d\)\+\>"

    call s:hif('skiaFuncCall', s:my_aqua)
    call s:hif('cppField', s:field)
    call s:hifs('cppFunctionCall', s:func, s:bold)
    call s:hif('cppConstant', s:enum)
    call s:hif('cppAccessor', s:my_aqua)
    call s:hif('enumConsts', s:macro)
endfunction

function! s:add_more_d_syntax()
  syn match dField "\v<m\u\w*>"
  syn match dFuncCall "\v<\l\w*\ze\("
  syn match dClass "\v<\u\w*>"
  syn match enumConsts "\u\(\u\|_\|\d\)\+\>"

  call s:hif('dField', s:field)
  call s:hifs('dFuncCall', s:func, s:bold)
  call s:hifs('dClass', s:type, s:bold)
  call s:hif('enumConsts', s:enum)
endfunction

function! s:add_js_syntax()
  syn match jsNumber "\v<\d+>"
  syn match jsNumber "\v<0x\w+>"
  hi def link jsNumber Number

  syn match jsFloat "\v<(\d*)\.(\d*)(f?)>"
  syn match jsFloat "\v<\.(\d*)(f?)>"
  hi def link jsFloat Float

  syn match nnClass "\v<\u\w*>"
  hi def link nnClass UserType
endfunction

augroup new_cpp_d_syntax
    autocmd!
    autocmd Filetype javascript call s:add_js_syntax()
    autocmd Filetype c,cpp,javascript call s:add_common_syntax()
    autocmd Filetype c,cpp,javascript call s:add_more_cpp_syntax()
    autocmd Filetype d call s:add_common_syntax()
    autocmd Filetype d call s:add_more_d_syntax()
augroup END

" }}}
